
#include <stdio.h>
#include <math.h>

#include "wg.h"
#include "distpluck.h"

// note: Zmax is defined in header as a macro
//       needed by linear-interpolated alpha defined in wg.c
static const float bow_k = 100.0;
static const float damping_factor = 0.05;

// State
static point_t bow, prevbow;
static int was_touching = 0;
static float w=0;
static float omega = 0;
static float pulse = 0;

// Options
int alpha_continuous = 0;

static float (*alpha)(float z);

int init_distpluck(int requested_sample_rate, float string_frequency)
{
    init_constants(requested_sample_rate, string_frequency);

    bow.x = 0;
    bow.v = 0;

    prevbow.x = 0;
    prevbow.v = 0;

    was_touching = 0;
    w=0;
    omega = 0;

    //alpha_continuous = 0;
    if (alpha_continuous) {
        //alpha = alpha_smooth;
        init_alpha();
        alpha = piecewise_alpha;
    }
    else
        alpha = alpha_fast;

    return sampleRate;
}

void step_distpluck(float pos[2], float force[2], float sound[2],
                    float vn[1], float xs[1], float vel[2])
{
    // pressure is vertical component converted to milligrams
    const int touching = pos[1] < 0;
    float bf = -pos[1] * touching * 100;

    // limit the force because at hard pressure it goes unstable
    if (bf >  1) bf =  1;
    if (bf < -1) bf = -1;
    const float bowforce = bf;

    // estimate bow position and velocity
    bow.x = pos[0];
    const float yk = bow.x - prevbow.x;
    bow.v = yk * sampleRate;
    prevbow.x = bow.x;

    // determine new touching state
    if (touching && !was_touching) {
        w = bow.x;
    }
    was_touching = touching;

    // H-A friction
    const float z = bow.x - w;
    float zsign = z<0?-1:1;
    w += alpha(z)*Zmax*(z-Zmax*zsign);
    w += omega / sampleRate;

    // friction force (tangential bow force)
    const float friction = -z * bowforce;

    // update string force wave
    float r=0, l=0;
    string_waveguide_get(&r, &l);
    const float string_vel = r+l;

    // new force to inject into waveguide
    float lambda = 6e-08;
    float gamma = 4*lambda*(string_freq*string_freq)
        / (betaratio-betaratio*betaratio);
    float newVel = -gamma
        * alpha(z) * Zmax * zsign
        * bowforce * touching;

    const float damping = 1-bowforce*damping_factor;
    string_waveguide_put(l*damping+newVel, r*damping+newVel);

    // move W according to string force
    omega = string_vel;

    sound[0] = sound[1] = string_vel;
    vn[0] = newVel;
    xs[0] = w;
    vel[0] = bow.v;
    vel[1] = 0;
    force[0] = friction;
    force[1] = -bowforce*0.001;  /* this scaling is related to the
                                    fact that I still need to figure
                                    out vertical force units. */
}
