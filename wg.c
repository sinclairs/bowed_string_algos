
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "wg.h"

int total_length;
float sampleRate;
float h;
float string_freq;

float *right_delay;
int right_delay_len = 0;
int right_delay_pos = 0;

float *left_delay;
int left_delay_len = 0;
int left_delay_pos = 0;

const float betaratio = 1-0.127236;

const float pi = 3.14159265359;

onepole_t string_filter;

void string_waveguide_get(float *r, float *l)
{
    *r = -(right_delay[right_delay_pos]);
    *l = -onepole(left_delay[left_delay_pos], &string_filter);
}

void string_waveguide_put(float r, float l)
{
    right_delay[right_delay_pos] = r;
    left_delay[left_delay_pos]   = l;
    right_delay_pos = (right_delay_pos+1)%right_delay_len;
    left_delay_pos = (left_delay_pos+1)%left_delay_len;
}

void onepole_set_gain(float gain, onepole_t *filter)
{
    filter->gain = gain;
}

void onepole_set_pole(float pole, onepole_t *filter)
{
    filter->pole = pole;
    filter->b0 = (filter->pole > 0)
        ? (1-filter->pole)
        : (1+filter->pole);
    filter->a1 = -filter->pole;
}

void onepole_init(onepole_t *filter)
{
    onepole_set_gain(0.99, filter);
    onepole_set_pole(0.6-(0.1*0.5), filter);
    filter->x0 = 0;
    filter->y1 = 0;
}

float onepole(float x, onepole_t *filter)
{
    filter->x0 = x*filter->gain;
    filter->y1 = filter->x0*filter->b0 - filter->y1*filter->a1;
    return filter->y1;
}

void biquad_set_cutoff_lowpass(float freq, biquad_t *filter)
{
    filter->cutoff = freq;

    // Low pass calculations from audio EQ cookbook.
    float w0 = 2*pi*freq/sampleRate;
    float dBgain = 0;
    float A = sqrt(pow(10, dBgain/20.0));
    float S = 1;
    float alpha = sin(w0)/2 * sqrt((A+1/A)*(1/S-1)+2);

    float b0 = (1 - cos(w0))/2;
    float b1 = 1 - cos(w0);
    float b2 = b0;
    float a0 = 1 + alpha;
    float a1 = -2*cos(w0);
    float a2 = 1 - alpha;

    // normalize coefficients such that a0=1
    filter->b0 = b0/a0;
    filter->b1 = b1/a0;
    filter->b2 = b2/a0;
    filter->a1 = a1/a0;
    filter->a2 = a2/a0;
}

void biquad_init(biquad_t *filter)
{
    biquad_set_cutoff_lowpass(200, filter);
    filter->x0 = 0;
    filter->x1 = 0;
    filter->x2 = 0;
    filter->y0 = 0;
    filter->y1 = 0;
    filter->y2 = 0;
}

float biquad(float x, biquad_t *filter)
{
    filter->x2 = filter->x1;
    filter->x1 = filter->x0;
    filter->x0 = x;
    filter->y2 = filter->y1;
    filter->y1 = filter->y0;
    filter->y0 =
        filter->x0*filter->b0
        + filter->x1*filter->b1
        + filter->x2*filter->b2
        - filter->y1*filter->a1
        - filter->y2*filter->a2;
    return filter->y0;
}

float bow_table(float x, float pressure)
{
    float slope = 5.0 - 4.0 * pressure;

    x = fabsf( x * slope ) + 0.75f;
    //x = pow( x, -4.0f );
    x = piecewise_pow4( x );

    if ( x > 1.0 ) x = 1.0f;

    if (pressure < 0.05)
        x *= pressure * 20;

    return x;
}

void init_constants(float requested_sample_rate, float string_frequency)
{
    sampleRate = requested_sample_rate;
    h = 1.0/sampleRate;

    string_freq = ((float)sampleRate) / (int)(sampleRate / string_frequency);

    total_length = sampleRate / string_freq;

    right_delay = malloc(sizeof(float)*total_length);
    left_delay = malloc(sizeof(float)*total_length);
    memset(right_delay, 0, sizeof(float)*total_length);
    memset(left_delay, 0, sizeof(float)*total_length);

    onepole_init(&string_filter);

    left_delay_len = (1-betaratio) * total_length;
    right_delay_len = betaratio * total_length;
}

static float *buffer_pow4=0;
static const int samples_pow4=99;
static const float pow4_min = 0.75f;
static const float pow4_max = 51.0f;

// This is where the sampling rate changes for pow(x,-4) function
// First half of buffer dedicated to (min,split), second half to (split,max)
static const float pow4_split = 2.0f;

static int getbin_pow4(float x)
{
    int bin;
    if (x < pow4_split)
        bin = (x-pow4_min) / (pow4_split-pow4_min) * (samples_pow4/2);
    else
        bin = ( (x-pow4_min-pow4_split) / (pow4_max-pow4_min-pow4_split)
                * (samples_pow4/2) + (samples_pow4/2) );
    return bin;
}

static float inversebin_pow4(int bin)
{
    float x;
    if (bin < samples_pow4/2)
        x = bin/((float)samples_pow4/2)*(pow4_split-pow4_min) + pow4_min;
    else
        x = ( (bin-samples_pow4/2)/((float)samples_pow4/2)
              * (pow4_max-pow4_min-pow4_split) + pow4_min + pow4_split );
    return x;
}

void init_pow4()
{
    int i;

    buffer_pow4 = (float*)malloc(sizeof(float*)*(samples_pow4+1));

    for (i=0; i<(samples_pow4+1); i++) {
        buffer_pow4[i] = pow(inversebin_pow4(i), -4.0f);
    }
}

float piecewise_pow4(float x)
{
    int left = getbin_pow4(x);
    int right = left+1;
    float leftx, rightx, ratio;

    if (x < 0.75)
        return buffer_pow4[0];

    if (x >= 51.0)
        return buffer_pow4[samples_pow4];

    leftx = inversebin_pow4(left);
    rightx = inversebin_pow4(right);

    ratio = (x-leftx)/(rightx-leftx);

    float f = buffer_pow4[left]*(1-ratio) + buffer_pow4[right]*ratio;

    return f;
}

static const int samples_alpha = 99;
static const float alpha_min = Zmax - 0.0001;
static const float alpha_max = Zmax + 0.0001;
static float *buffer_alpha = 0;

float alpha_fast(float z)
{
    if (fabs(z) >= Zmax)
        return 1.0f/Zmax;
    else
        return 0;
}

float alpha_smooth(float z)
{
    const float power = 40;
    return 1.0/Zmax * pow(z,power)/(pow(Zmax, power)+pow(z,power));
}

static int getbin_alpha(float x)
{
    return (int)((x-alpha_min)/(alpha_max-alpha_min)*samples_alpha);
}

static float inversebin_alpha(int bin)
{
    return bin/((float)samples_alpha)*(alpha_max-alpha_min)+alpha_min;
}

void init_alpha()
{
    int i;

    buffer_alpha = (float*)malloc(sizeof(float*)*(samples_alpha+2));

    for (i=0; i<(samples_alpha+2); i++) {
        buffer_alpha[i] = alpha_smooth(inversebin_alpha(i));
    }

    buffer_alpha[0] = 0;
    buffer_alpha[samples_alpha+1] = 1/Zmax;
}

float piecewise_alpha(float z)
{
    z = fabs(z);
    if (z < alpha_min)
        return 0;
    if (z > alpha_max)
        return 1/Zmax;

    int left = getbin_alpha(z);
    int right = left + 1;

    float leftx = inversebin_alpha(left);
    float rightx = inversebin_alpha(right);

    float ratio = (z-leftx)/(rightx-leftx);

    return buffer_alpha[left]*(1-ratio) + buffer_alpha[right]*ratio;
}
