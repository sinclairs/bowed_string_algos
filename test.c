
#include "wg.h"
#include <stdio.h>

void test_pow4()
{
    int i;
    init_pow4();
    for (i=0; i<1000; i++) {
        float d = (i/1000.0*(51.0-0.75)+0.75);
        printf("%f, ", d);
        fflush(stdout);
        float f = piecewise_pow4(d);
        printf("%f\n", f);
    }
}

void test_alpha()
{
    int i;
    init_alpha();
    for (i=-1000; i<1000; i++) {
        float d = (i/1000.0*Zmax*2);
        printf("%f, ", d);
        fflush(stdout);
        float f = piecewise_alpha(d);
        printf("%f\n", f);
    }
}

void test_bowtable()
{
    int i;
    init_pow4();
    for (i=-10000; i<10000; i++) {
        float d = (i/10000.0*2);
        printf("%f, ", d);
        fflush(stdout);
        float f = bow_table(d, 0.7);
        printf("%f\n", f);
    }
}

int main()
{
    test_pow4();
    return 0;
}
