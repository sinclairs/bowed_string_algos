
#define GLUT_DISABLE_ATEXIT_HACK
#include <chai3d/chai3d.h>

#include <stdio.h>
#include <signal.h>

// The ALGONAME macro should be defined on the command line when
// compiling this file.
#define ALGO_(x,a) x##_##a
#define ALGO__(x,a) ALGO_(x,a)
#define ALGO(x) ALGO__(x, ALGONAME)

#include "../wg.h"
#include "../twopoint.h"
#include "../velstate.h"
#include "../distpluck.h"

int mu_k=1;
int newalgo=0;
float variables[4];
int   record=0;
float damping=0;
int   dump_ptr=0;
int   dump_size=0;

int   *p_mu_k = &mu_k;
int   *p_newalgo = &newalgo;
float *p_variables = &variables[0];
int   *p_record = &record;
float *p_damping = &damping;
int   *p_dump_ptr = &dump_ptr;
int   *p_dump_size = &dump_size;

float sf_pole = 0.5, *p_sf_pole = &sf_pole;
float sf_gain = 0.999, *p_sf_gain = &sf_gain;
float frclp_freq = 0, *p_frclp_freq = &frclp_freq;
float sndlp_freq = 0, *p_sndlp_freq = &sndlp_freq;
float vellp_freq = 0, *p_vellp_freq = &vellp_freq;

/* AUDIO */
#include <RtAudio.h>
#include <pthread.h>
RtAudio *rtaudio = 0;

#define ABUFSZ 4096
double audio_buffer[ABUFSZ];
int audio_write = -1;
int audio_read = 0;

// for synchronization between audio and haptics
pthread_cond_t cv; 
pthread_mutex_t mutex; 
int first=0;
double last_t = 0;
/* END AUDIO */

void msg_host(int, char* s, ...)
{
}

bool quit = false;

void on_quit(int)
{
    quit = true;
}

void do_sleep()
{
#ifdef WIN32
    Sleep(1);
#else
    struct timeval tv;
    gettimeofday(&tv, 0);
    static double last_msec = 0;
    double msec = tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0;
    double msec_diff = msec - last_msec;

    if (msec_diff < 1) {
        struct timespec req;
        req.tv_sec = 0;
        req.tv_nsec = (int)((1 - msec_diff)*1000000);
        nanosleep(&req, 0);
        gettimeofday(&tv, 0);
        msec = tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0;
    }

    last_msec = msec;
#endif
}

int play( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
          double streamTime, RtAudioStreamStatus status, void *userData )
{
    unsigned int i, j;
    double *buffer = (double *) outputBuffer;
    double *lastValues = (double *) userData;

#ifndef WIN32
    // setting higher priority reduces timing spikes
    if (!first) {
        struct sched_param mysched;
        mysched.sched_priority = 99;
        if (sched_setscheduler( 0, SCHED_FIFO, &mysched ) == -1)
            printf("Couldn't set priority scheduling.\n");
        first = 1;
    }
#endif

    if ( status )
        std::cout << "Stream underflow detected!" << std::endl;

    // Write interleaved audio data.
    for ( i=0; i<nBufferFrames*2; i++ ) {
        if ( ((audio_read+1)%ABUFSZ) == audio_write ) {
            // wait for "more data" signal from haptic loop
            pthread_cond_wait(&cv, &mutex);
            // try again
            i--;
        } else {
            double a = audio_buffer[audio_read];
            if (a >  1.0) a =  1.0;
            if (a < -1.0) a = -1.0;
            *buffer++ = a;
            audio_read = (audio_read+1)%ABUFSZ;

	    // other channel
	    //*buffer++ = *(buffer-1);
        }
    }

    return 0;
}

void audio_add_sample(double sample)
{
    audio_buffer[audio_write] = sample;
    audio_write = (audio_write + 1) % ABUFSZ;
}

void init_audio()
{
    rtaudio = new RtAudio();
    //Stk::setSampleRate(48000);

    RtAudio::StreamParameters parameters;
    parameters.deviceId = rtaudio->getDefaultOutputDevice();
    parameters.nChannels = 2;
    parameters.firstChannel = 0;
    unsigned int sampleRate = 48000;
    //unsigned int bufferFrames = 192;
    unsigned int bufferFrames = 512;

    RtAudio::StreamOptions options;
    options.flags = RTAUDIO_SCHEDULE_REALTIME;
    options.numberOfBuffers = 2;
    options.priority = SCHED_FIFO;

    memset(audio_buffer, 0, sizeof(double)*ABUFSZ);

    pthread_mutex_lock(&mutex);

    try {
        rtaudio->openStream( &parameters, NULL, RTAUDIO_FLOAT64,
                             sampleRate, &bufferFrames, &play, &options );
        rtaudio->startStream();
    }
    catch ( RtError& e ) {
        e.printMessage();
        delete rtaudio;
        rtaudio = 0;
    }
}

void cleanup_audio()
{
    if (rtaudio)
        delete rtaudio;
    pthread_mutex_unlock(&mutex);
}

int main()
{
    cHapticDeviceHandler *h = new cHapticDeviceHandler();
    cGenericHapticDevice *dev;
    printf("Haptic devices found: %d\n", h->getNumDevices());

    int r = h->getDevice(dev, 0);
    if (r<0) {
      printf("h->getDevice() returned %d\n", r);
      return 1;
    }
    r = dev->open();
    if (r<0) {
      printf("dev->open() returned %d\n", r);
      return 1;
    }
    r = dev->initialize();
    if (r<0) {
      printf("dev->open() returned %d\n", r);
      return 1;
    }

    init_audio();

    signal(SIGINT, on_quit);

    printf("Initializing.\n");
    printf("Sample rate: %d\n", ALGO(init)(48000, 220.0f));

    printf("Running.\n");
    while (!quit)
    {
        cVector3d pos;
        dev->getPosition(pos);
        if (pos.z < -0.01) pos.z = -0.01;
        cVector3d force;
        force.zero();
        float pos2d[2], frc2d[2] = {0.0, 0.0}, snd2d[2] = {0.0, 0.0};
        float vn[1], xs[1], vel[2];

	int i;
	for (i=0; i<(48000/1000); i++)
	{
	  pos2d[0] = (float)(pos.y*1.0);
	  pos2d[1] = (float)(pos.z*1.0);
	  frc2d[0] = 0;
	  frc2d[1] = 0;
	  ALGO(step)(pos2d, frc2d, snd2d, vn, xs, vel);
	  audio_add_sample(snd2d[0]);
	  audio_add_sample(snd2d[1]);
	}

	if (isnan(frc2d[0] || isnan(frc2d[1])))
	  force[0] = force[1] = force[2] = 0;
	else {
	  force.y = frc2d[0]*10000.0;
	  force.z = frc2d[1]*-1000.0*5;
	}

#if 1
        static int c=0;
        if (c++ > 100) {
        printf("pos: %2.3f, %2.3f, %2.3f   frc: %2.3f, %2.3f, %2.3f        \r",
               pos[0], pos[1], pos[2], force[0], force[1], force[2]);
        fflush(stdout);
        c=0;
        }
#endif

        dev->setForce(force);

        /* timer function */
        do_sleep();
    }

    cleanup_audio();

    return 0;
}
