
#include <stdio.h>

#include "wg.h"
#include "twopoint.h"
#include "foaw.h"

// note: Zmax is defined in header as a macro
//       needed by linear-interpolated alpha defined in wg.c

// State
static point_t bow, prevbow;
static float sticking_point = 0;
static float virtual_point = 0;
static int sticking = 0;

#define ENDFIT_SIZE 7
static float posbuf[ENDFIT_SIZE];
static int posbufpos = 0;

float calc_velocity(float pos)
{
#if 0
    static float prevpos = pos;
    float v = (pos - prevpos) * sampleRate;
    prevpos = pos;
#endif

#if 1
    float v = do_foaw_sample(posbuf, ENDFIT_SIZE, &posbufpos, pos, 0,
                             1.0f/sampleRate, 0.0001);
#endif

    return v;
}

int init_twopoint(int requested_sample_rate, float string_frequency)
{
    init_constants(requested_sample_rate, string_frequency);
    init_pow4();

    bow.x = 0;
    bow.v = 0;

    prevbow.x = 0;
    prevbow.v = 0;

    sticking_point = 0;
    virtual_point = 0;
    sticking = 0;

    return sampleRate;
}

void step_twopoint(float pos[2], float force[2], float sound[2],
                   float vn[1], float xs[1], float vel[2])
{
    // pressure is vertical component converted to milligrams
    const int touching = pos[1] < 0;
    float bf = -pos[1] * touching * 10;

    // limit the force because at hard pressure it goes unstable
    if (bf >  1) bf =  1;
    if (bf < -1) bf = -1;
    const float bowforce = bf;

    // Set H-A virtual point
    if (touching) {
        if (!sticking) {
            sticking_point = virtual_point = pos[0];
            sticking = 1;
            prevbow.x = virtual_point;
        }
    }
    else
        sticking = 0;

    // estimate bow position and velocity
    bow.x = virtual_point;
    bow.v = calc_velocity(bow.x);

    // update string force wave
    float r=0, l=0;
    string_waveguide_get(&r, &l);
    const float stringVel = r+l;
    const float mu = bow_table(bow.v - stringVel, bowforce);
    const float newVel = (bow.v - stringVel) * mu;
    string_waveguide_put(l + newVel, r + newVel);

    // H-A stick-slide friction
    const float Z = pos[0] - virtual_point;
    const float sign = Z<0 ? -1 : 1;

    if (Z*sign > Zmax && touching) {
        virtual_point = pos[0] - Zmax*sign;
        sticking_point = virtual_point - (virtual_point-sticking_point)*mu;
    }

    // Assert a maximum on the sticking force
    float sticking_K = bowforce * mu;
    if (sticking_K > 3) sticking_K = 3;

    // friction force (tangential bow force)
    const float friction = (pos[0]-sticking_point) * sticking_K * touching;

    sound[0] = sound[1] = stringVel;
    vn[0] = newVel;
    xs[0] = sticking_point;
    vel[0] = bow.v;
    vel[1] = 0;
    force[0] = -friction*10;
    force[1] = -bowforce*0.01;  /* this scaling is related to the
                                   fact that I still need to figure
                                   out vertical force units. */
}
