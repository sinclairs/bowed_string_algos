
#ifndef _DISTPLUCK_H_
#define _DISTPLUCK_H_

extern int alpha_continuous;

int init_distpluck(int requested_sample_rate, float string_frequency);

void step_distpluck(float pos[2], float force[2], float sound[2],
                    float vn[1], float xs[1], float vel[2]);

#endif // _DISTPLUCK_H_
