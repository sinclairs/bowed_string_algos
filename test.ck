
BlitSaw s => dac;

10 => s.harmonics;

1.0/(135/30000.0) => s.freq;
<<<s.freq()>>>;

2::second => now;

1.0/(134/30000.0) => s.freq;
<<<s.freq()>>>;

now + 1::second => time end;

while (now < end) {
  0.02::second => now;
  s.gain() * 0.95 => s.gain;
}
