
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "wg.h"
#include "velstate.h"
#include "distpluck.h"
#include "stk.h"
#include "twopoint.h"

typedef int init_func(int requested_sample_rate, float string_frequency);

typedef void step_func(float pos[2], float force[2], float sound[2],
                       float vn[1], float xs[1], float vel[2]);

float fields[10] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
int field_order[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

float *pos = &fields[0];
float *frc = &fields[2];
float *snd = &fields[4];
float *vn  = &fields[6];
float *xs  = &fields[7];
float *vel = &fields[8];

const char *field_ids[] = {"posx", "posy", "frcx", "frcy", "sndl",
                           "sndr","vn","xs","velx","vely"};
const int n_fields = sizeof(field_ids)/sizeof(char*);

const float target_velocity = 0.03;
const float bowforce = 0.01;
const float rate = 44100;
const float oversample = 1;

void parse_fields(char *str)
{
    char *s = str, *p = s;
    int n, *pf=&field_order[0];
    while (*s) {
        while (*s && (*s)!=',') s++;
        for (n=0; n < n_fields; n++)
            if (strncmp(p, field_ids[n], s-p)==0) {
                *pf++ = n;
                break;
            }
        if (n==n_fields) {
            fprintf(stderr, "Unknown field.\n");
            exit(1);
        }
        if (*s) s++;
        p = s;
    }
}

void enable_all_fields()
{
    int n;
    for (n=0; n < n_fields; n++)
        field_order[n] = n;
}

void print_fields()
{
    int n;
    if (field_order[0] != -1) {
        printf("%.12f", fields[field_order[0]]);
        for (n=1; n < n_fields; n++) {
            if (field_order[n] == -1)
                break;
            printf(", %.12f", fields[field_order[n]]);
        }
        printf("\n");
    }
}

void simloop_normal(step_func *step, int n_steps, int add_noise)
{
    int i,j;
    pos[0] = 0;
    pos[1] = -bowforce;
    for (i=0; i < n_steps; i++)
    {
        for (j=0; j < oversample; j++)
        {
            pos[0] = pos[0] + target_velocity/sampleRate;
            if (add_noise)
                pos[0] += (rand()/(float)RAND_MAX-0.5f)*2.0f*0.0000001f;

            (*step)(pos, frc, snd, vn, xs, vel);
        }

        print_fields();
    }
}

void simloop_bow_model(step_func *step, int n_steps, int add_noise)
{
    int i,j;
    float v = 0;
    pos[0] = 0;
    pos[1] = -bowforce;
    for (i=0; i < n_steps; i++)
    {
        for (j=0; j < oversample; j++)
        {
            if (v != target_velocity)
                v += (target_velocity - v)*0.01;

            float prevpos = pos[0];
            pos[0] = pos[0] + v/sampleRate;

            if (add_noise)
                pos[0] += (rand()/(float)RAND_MAX-0.5f)*2.0f*0.0000003f;

            if (i*oversample+j > sampleRate && pos[1] < 0.005) {
                pos[1] += 0.000001;
            }

            (*step)(pos, frc, snd, vn, xs, vel);
        }

        print_fields();
    }
}

int main(int argc, char *argv[])
{
    init_func *init=0;
    step_func *step=0;
    int n_steps = 10, i, n;
    int add_noise = 0, use_bow_model = 0;

    if (argc < 2)
        goto usage;

    for (i=1; i<argc; i++)
    {
        for (n=0; n<strlen(argv[i]); n++)
            if (!isdigit(argv[i][n]))
                break;
        if (n==strlen(argv[i]))
            break;

        if (strcmp(argv[i], "velstate")==0) {
            init = init_velstate;
            step = step_velstate;
        }
        else if (strcmp(argv[i], "distpluck")==0) {
            init = init_distpluck;
            step = step_distpluck;
        }
        else if (strcmp(argv[i], "stk")==0) {
            init = init_stk;
            step = step_stk;
        }
        else if (strcmp(argv[i], "twopoint")==0) {
            init = init_twopoint;
            step = step_twopoint;
        }
        else if (strcmp(argv[i], "cordis")==0) {
            fprintf(stderr, "cordis code was removed due to copyright.\n");
            exit(1);
        }
        else if (strcmp(argv[i], "noise")==0) {
            add_noise = 1;
        }
        else if (strcmp(argv[i], "bow")==0) {
            use_bow_model = 1;
        }
        else if (strcmp(argv[i], "sahpulse")==0) {
            sample_and_hold_pulse = 1;
        }
        else if (strcmp(argv[i], "limitpulse")==0) {
            limit_pulse = 1;
        }
        else if (strcmp(argv[i], "alphacont")==0) {
            alpha_continuous = 1;
        }
        else {
            printf("Unknown algorithm selected.\n");
            goto usage;
        }
    }

    if (i < argc)
        n_steps = atoi(argv[i++]);

    if (i < argc)
        parse_fields(argv[i++]);
    else
        enable_all_fields();

    if (!init || !step)
        goto usage;

    sampleRate = (*init)(rate*oversample, 160.0f);

    if (use_bow_model)
        simloop_bow_model(step, n_steps, add_noise);
    else
        simloop_normal(step, n_steps, add_noise);

    goto done;

  usage:
    printf("Usage: sim <velstate|distpluck|stk|twopoint> [bow] [noise] "
           "[sahpulse] [limitpulse] [alphacont] "
           "[steps=10 [field,field,..]]\n");

  done:
    return 0;
}
