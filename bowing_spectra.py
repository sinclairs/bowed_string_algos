#!/usr/bin/env python

from pylab import *

rc('text',usetex=True)
rc('font',family='serif',serif='Computer Modern',size=24)

N = 10100
M = 30100
tp = loadtxt('tp.txt', delimiter=',')[N:M]
dp = loadtxt('dp.txt', delimiter=',')[N:M]
dpalpha = loadtxt('dpalpha.txt', delimiter=',')[N:M]

sr = 44100.0

def spec(x):
    n = sr/2 - len(x)
    if n>0:
        f = fft(concatenate((x * hanning(len(x)), zeros(n))))
    else:
        f = fft(x * hanning(len(x)))
    f = f[:len(f)/2]
    hz = arange(len(f))/float(len(f))*(sr/2)
    return hz, f

figure(1).clear()
figure(2).clear()

i = 0
for t,p in [('square $\\alpha$',dp), ('continuous $\\alpha$',dpalpha)]:
    figure(1)
    subplot(2,1,i+1)
    plot(arange(1000)/sr, p[:1000,1], 'k')
    ylabel('\\centerline{%s}\centerline{Velocity (m/s)}'%t)
    ylim(-0.6, 0.4)
    figure(2)
    subplot(2,1,i+1)
    s = spec(p[:,1])
    plot(s[0], 20*log10(s[1]), 'k')
    xlim(0,sr/2)
    ylim(-100,40)
    ylabel('Magnitude (dB)')
    # subplot(3,1,i+3)
    # plot(p[:,1])
    # subplot(3,1,i+4)
    # s = spec(p[:,1])
    # semilogy(s[0], s[1])
    # xlim(0,sr/2)
    i += 1

figure(1)
xlabel('Time (s)')

figure(2)
xlabel('Frequency (Hz)')
