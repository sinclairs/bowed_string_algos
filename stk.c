
#include <stdio.h>

#include "wg.h"
#include "stk.h"

static const float bow_k = 100.0;
static const float damping_factor = 0.05;

// State
static point_t bow, prevbow;
static int was_touching = 0;
static float w=0;

int init_stk(int requested_sample_rate, float string_frequency)
{
    init_constants(requested_sample_rate, string_frequency);
    init_pow4();

    bow.x = 0;
    bow.v = 0;

    prevbow.x = 0;
    prevbow.v = 0;

    was_touching = 0;
    w=0;

    return sampleRate;
}

void step_stk(float pos[2], float force[2], float sound[2],
              float vn[1], float xs[1], float vel[2])
{
    // pressure is vertical component converted to milligrams
    const int touching = pos[1] < 0;
    float bf = -pos[1] * touching;

    // limit the force because at hard pressure it goes unstable
    if (bf >  1) bf =  1;
    if (bf < -1) bf = -1;
    const float bowforce = bf;

    // estimate bow position and velocity
    bow.x = pos[0];
    bow.v = (bow.x - prevbow.x) * sampleRate;
    prevbow.x = bow.x;

    // determine new touching state
    if (touching && !was_touching) {
        w = bow.x;
    }
    was_touching = touching;

    // H-A friction
    const float zw = (bow.x - w);
    const int moved = (zw > Zmax) || (zw < -Zmax);

    if (zw > Zmax) {
        w = bow.x - Zmax;
    }
    else if (zw < -Zmax) {
        w = bow.x + Zmax;
    }

    // friction force (tangential bow force)
    const float friction = (bow.x - w)*( bow_k*(1-moved)*0.5
                                         + bow_k*(moved) );

    // update string force wave
    float r=0, l=0;
    string_waveguide_get(&r, &l);
    const float stringVel = r+l;

    const float newVel = (bow.v - stringVel) * bow_table(bow.v - stringVel, bowforce);

    string_waveguide_put(l + newVel, r + newVel);

    sound[0] = sound[1] = stringVel;
    vn[0] = newVel;
    vel[0] = bow.v;
    vel[1] = 0;
    force[0] = friction;
    force[1] = bowforce;
}
