
#include <stdio.h>
#include <math.h>

#include "wg.h"
#include "velstate.h"

static const float damping_factor = 0.05;

static const float friction_k = 100.0f;  // related to bow force range

// State
static point_t bow, prevbow;
static int was_touching = 0;
static int sticking = 0;
static float w=0, y=0;
static float omega = 0;
static int moved_last_time = 0;
static float pulse = 0;

// Options
int sample_and_hold_pulse = 0;
int limit_pulse = 0;

int init_velstate(int requested_sample_rate, float string_frequency)
{
    init_constants(requested_sample_rate, string_frequency);
    init_pow4();

    bow.x = 0;
    bow.v = 0;

    prevbow.x = 0;
    prevbow.v = 0;

    was_touching = 0;
    sticking = 0;
    w=0;
    y=0;
    omega = 0;

    return sampleRate;
}

void step_velstate(float pos[2], float force[2], float sound[2],
                   float vn[1], float xs[1], float vel[2])
{
    // pressure is vertical component converted to milligrams
    const int touching = pos[1] < 0;
    float bf = -pos[1] * touching;

    // limit the force because at hard pressure it goes unstable
    if (bf >  1) bf =  1;
    if (bf < -1) bf = -1;
    const float bowforce = bf;

    // estimate bow position and velocity
    bow.x = pos[0];
    bow.v = (bow.x - prevbow.x) * sampleRate;
    prevbow.x = bow.x;

    // determine new touching state
    if (touching && !was_touching) {
        w = bow.x;
        y = bow.x;
        sticking = 1;
    }
    was_touching = touching;

    // H-A friction
    const float zw = (bow.x - w);
    const int moved = (zw > Zmax) || (zw < -Zmax);
    float w_displacement = w;

    if (zw > Zmax) {
        w = bow.x - Zmax;
    }
    else if (zw < -Zmax) {
        w = bow.x + Zmax;
    }

    w_displacement = w - w_displacement;

    w += omega / sampleRate;

    // H-A friction (direction detector)
    const float zy = (bow.x - y);
    if (zy > Zmax) {
        y = bow.x - Zmax;
    }
    else if (zy < -Zmax) {
        y = bow.x + Zmax;
    }

    // friction force (tangential bow force)
    const float friction = -(bow.x*2 - w - y)
        * ( friction_k*(1-moved)*0.5
            + friction_k*(moved) )
        * bowforce;

    // update string force wave
    float r=0, l=0;
    string_waveguide_get(&r, &l);
    const float string_vel = r+l;

    if (sticking) {
        // unsticking occurs if string moves far enough away from bow
        // point such that it needs to be moved back into alignment
        if (moved && !moved_last_time)
            sticking = 0;
    }
    else {
        // resticking occurs if string is close enough to the bow
        if (!moved)
            sticking = 1;
    }

    // new force to inject into waveguide
    const int sign = ((bow.x - y) > 0) ? 1 : -1;
    float newVel=0;

    // a velocity pulse equal to string displacement
    if (!sample_and_hold_pulse || fabs(w_displacement)*sampleRate > pulse)
        pulse = fabs(w_displacement)*sampleRate*touching;

    if (limit_pulse)
        if (pulse > 0.0005*sampleRate/total_length)
            pulse = 0.0005*sampleRate/total_length;

    // outside Zmax region means slipping: keep w within range using
    // omega offset, but add a pulse to the waveguide
    if (sticking)
        pulse = 0;
    else
        newVel = -pulse*sign;

    const float damping = 1-(1-moved)*bowforce*damping_factor;
    string_waveguide_put(l*damping+newVel, r*damping+newVel);

    // move W according to string force
    omega = string_vel;
    moved_last_time = moved;

    sound[0] = sound[1] = string_vel;
    vn[0] = newVel;
    xs[0] = w;
    vel[0] = bow.v;
    vel[1] = 0;
    force[0] = friction;
    force[1] = -bowforce*0.1;  /* this 0.1 scaling is related to the
                                  fact that I still need to figure out
                                  vertical force unit scaling. */
}
