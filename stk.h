
#ifndef _STK_H_
#define _STK_H_

int init_stk(int requested_sample_rate, float string_frequency);

void step_stk(float pos[2], float force[2], float sound[2],
              float vn[1], float xs[1], float vel[2]);

#endif // _STK_H_
