
CC = gcc
CFLAGS = -g -O0
LDLIBS = -lm

sim: sim.o wg.o velstate.o distpluck.o stk.o twopoint.o

test: test.o wg.o

.PHONY: clean
clean:
	-@rm -fv sim *.o
