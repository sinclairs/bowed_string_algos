
#ifndef _WG_H_
#define _WG_H_

typedef struct _onepole
{
    float pole;
    float gain;
    float b0;
    float a1;
    float x0;
    float y1;
} onepole_t;

typedef struct _biquad
{
    float cutoff;
    float b0, b1, b2;
    float a1, a2;
    float x0, x1, x2;
    float y0, y1, y2;
} biquad_t;

typedef struct _point
{
    float x;
    float v;
} point_t;

extern int total_length;
extern float sampleRate;

void string_waveguide_get(float *r, float *l);
void string_waveguide_put(float r, float l);

void onepole_set_gain(float gain, onepole_t *filter);
void onepole_set_pole(float pole, onepole_t *filter);
void onepole_init(onepole_t *filter);
float onepole(float x, onepole_t *filter);

void biquad_set_cutoff_lowpass(float freq, biquad_t *filter);
void biquad_init(biquad_t *filter);
float biquad(float x, biquad_t *filter);

float bow_table(float x, float pressure);

#define Zmax 0.0005

void init_pow4();
float piecewise_pow4(float x);

void init_alpha();
float piecewise_alpha(float z);

float alpha_fast(float z);
float alpha_smooth(float z);

void init_constants(float requested_sample_rate, float string_frequency);

extern const float betaratio;
extern float string_freq;

#endif // _WG_H_
