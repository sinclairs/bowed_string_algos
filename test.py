
from pylab import *

Zmax = 0.0005

def alpha1(z):
    if abs(z) > Zmax:
        return 1.0/Zmax
    else:
        return 0

def alpha2(z, power=8):
    Zstick = Zmax * 1.1
    return 1.0/Zstick * pow(z,power)/(pow(Zmax, power)+pow(z,power))

z = arange(-Zmax*2,Zmax*2,Zmax*0.01)

clf()
plot(z, [alpha1(x) for x in z])
plot(z, [alpha2(x) for x in z])
plot(z, [alpha2(x,500) for x in z])

show()
