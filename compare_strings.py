#!/usr/bin/env python

from pylab import *
from scipy.signal import *
from scikits.audiolab import play
from scipy.io import loadmat
import os

rc('legend',fontsize=8)

models = ['stk','distpluck','distpluck alphacont','cordis']

sr = 44100.0

signals = [loadtxt(os.popen('./sim %s bow 80000 sndr,posy,vn'%m), delimiter=',')
           for m in models]

M = 35000
N = 10000

hh = loadmat('bass_bar/hh.mat')['indata'][22:5000,1]
hh = hh/sum(hh)

conved = [0]*len(signals)
fundamentals = [0]*len(signals)

figure(1).clear()
figure(2).clear()
figure(3).clear()

def parabola_max((x1,x2,x3), (y1,y2,y3)):
    """Given three points, calculate the location of the maximum (or
    minimum) of a fitted parabola."""
    denom = (x1-x2)*(x1-x3)*(x2-x3)
    A = (x3*(y2-y1) + x2*(y1-y3) + x1*(y3-y2)) / denom
    B = (x3**2*(y1-y2) + x2**2*(y3-y1) + x1**2*(y2-y3)) / denom
    C = (x2*x3*(x2-x3)*y1 + x3*x1*(x3-x1)*y2 + x1*x2*(x1-x2)*y3) / denom
    maxx = -B/(2*A)
    maxy = A*maxx**2 + B*maxx + C
    return maxx,maxy

for n,s in enumerate(signals):
    if 'distpluck' in models[n]:
        s[:,0] = detrend(lfilter([1],[1,-1],s[:,0]))

    figure(1)

    t = arange(0,len(s))/sr
    subplot(len(signals),2,n*2+1)
    plot(t[M:M+N], s[M:M+N])
    #pl(s)

    c = fftconvolve(hh, s[:,0])
    conved[n] = c
    # plot(t[M:M+N], c[M:M+N])

    xlim(M/sr, (M+N)/sr)

    ylabel(models[n].replace(' ','\n'), rotation=0)
    xlabel('Time (s)')

    subplot(len(signals),2,n*2+2)
    w = concatenate((detrend(s[M:M+N,0])*hanning(N), zeros(max(0,sr/2-N))))
    f = abs(fft(w)[:len(w)/2])/N
    hz = arange(0,len(f))/float(len(f))*(sr/2)
    index = lambda i: float(i)/(sr/2)*len(f)
    k = index(2000)
    m = index(110)
    plot(hz[:k], 20*log10(f[:k]/f[m]), 'k')
    ylabel('dB',rotation=0)
    xlabel('Hz')
    ylim(-150,50)

    figure(2)
    #subplot(211)
    plot(hz[:k], 20*log10(f[:k]/f[m]), label=models[n],
         color='krby'[n])

    figure(1)
    w = concatenate((detrend(c[M:M+N])*hanning(N), zeros(max(0,sr/2-N))))
    f = abs(fft(w)[:len(w)/2])/N
    hz = arange(0,len(f))/float(len(f))*(sr/2)
    semilogy(hz[:k], f[:k])

    figure(2)
    subplot(212)
    semilogy(hz[:k], f[:k]/f[m], label=models[n])

    N = 2048
    hop = 16
    v = zeros(((len(s)-N)/hop, N))
    m = zeros(v.shape[0])
    h = hanning(N)

    for i in xrange(v.shape[0]):
        w = s[i*hop : i*hop + N, 0] * h
        v[i,:] = fftconvolve(w,w[::-1])[N-1:]
        d = argmax(v[i, 100:])+100
        if d >= v.shape[1]-2: d = v.shape[1]-2
        elif d < 2: d = 2
        mx, my = parabola_max((d-1,d,d+1), v[i, d-1:d+2])
        m[i] = 44100.0/mx if mx!=0 else 0
        if i>0 and m[i] > m[i-1]*1.9:
            m[i] /= 2

    figure(3)
    subplot(len(signals),2,n*2+1)
    imshow(v[2000:,:].T, origin='lower', aspect=v.shape[0]/float(v.shape[1])/3)
    subplot(len(signals),2,n*2+2)
    fundamentals[n] = m
    plot(m[2000:])
    plot(s[2000:,1])

figure(3)
clf()
subplot(211)
[plot(s[2000::16,1]) for s in signals]
subplot(212)
[plot(f[2000:]) for f in fundamentals]

figure(2)
legend(loc=1)
ylabel('Amplitude (dB)')
xlabel('Frequency (Hz)')

ion()
draw()

