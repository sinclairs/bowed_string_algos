
#define GLUT_DISABLE_ATEXIT_HACK
#include <chai3d/chai3d.h>

#include <stdio.h>
#include <signal.h>

// The ALGONAME macro should be defined on the command line when
// compiling this file.
#define ALGO_(x,a) x##_##a
#define ALGO__(x,a) ALGO_(x,a)
#define ALGO(x) ALGO__(x, ALGONAME)

#include "../wg.h"
#include "../twopoint.h"
#include "../velstate.h"
#include "../distpluck.h"

int mu_k=1;
int newalgo=0;
float variables[4];
int   record=0;
float damping=0;
int   dump_ptr=0;
int   dump_size=0;

int   *p_mu_k = &mu_k;
int   *p_newalgo = &newalgo;
float *p_variables = &variables[0];
int   *p_record = &record;
float *p_damping = &damping;
int   *p_dump_ptr = &dump_ptr;
int   *p_dump_size = &dump_size;

float sf_pole = 0.5, *p_sf_pole = &sf_pole;
float sf_gain = 0.999, *p_sf_gain = &sf_gain;
float frclp_freq = 0, *p_frclp_freq = &frclp_freq;
float sndlp_freq = 0, *p_sndlp_freq = &sndlp_freq;
float vellp_freq = 0, *p_vellp_freq = &vellp_freq;

/* AUDIO */
#include <RtAudio.h>
#include <pthread.h>
RtAudio *rtaudio = 0;

#define ABUFSZ 4096
double audio_buffer[ABUFSZ];
int audio_write = -1;
int audio_read = 0;

// for synchronization between audio and haptics
pthread_cond_t cv; 
pthread_mutex_t mutex; 
int first=0;
double last_t = 0;
/* END AUDIO */

/* RECORDING */
#define RECBUFSZ 65536
double rec_buffer[RECBUFSZ];
int rec_write = -1;
int rec_read = 0;
pthread_t rec_thread;
/* END RECORDING */

const int recording_buffer_size = 2*10*1000;
float *recording_buffer = (float*)malloc(recording_buffer_size*sizeof(float));
int recording_buffer_pos = 0;

const int aud_recording_buffer_size = 2*10*48000;
float *aud_recording_buffer = (float*)malloc(aud_recording_buffer_size*sizeof(float));
int aud_recording_buffer_pos = 0;

void msg_host(int, char* s, ...)
{
}

bool quit = false;

void on_quit(int)
{
    quit = true;
}

void do_sleep()
{
#ifdef WIN32
    Sleep(1);
#else
    struct timeval tv;
    gettimeofday(&tv, 0);
    static double last_msec = 0;
    double msec = tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0;
    double msec_diff = msec - last_msec;

    if (msec_diff < 1) {
        struct timespec req;
        req.tv_sec = 0;
        req.tv_nsec = (int)((1 - msec_diff)*1000000);
        nanosleep(&req, 0);
        gettimeofday(&tv, 0);
        msec = tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0;
    }

    last_msec = msec;
#endif
}

int play( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
          double streamTime, RtAudioStreamStatus status, void *userData )
{
    unsigned int i, j;
    double *buffer = (double *) outputBuffer;
    double *lastValues = (double *) userData;

#ifndef WIN32
    // setting higher priority reduces timing spikes
    if (!first) {
        struct sched_param mysched;
        mysched.sched_priority = 99;
        if (sched_setscheduler( 0, SCHED_FIFO, &mysched ) == -1)
            printf("Couldn't set priority scheduling.\n");
        first = 1;
    }
#endif

    if ( status )
        std::cout << "Stream underflow detected!" << std::endl;

    // Write interleaved audio data.
    for ( i=0; i<nBufferFrames*2; i++ ) {
        if ( ((audio_read+1)%ABUFSZ) == audio_write ) {
            // wait for "more data" signal from haptic loop
            pthread_cond_wait(&cv, &mutex);
            // try again
            i--;
        } else {
            double a = audio_buffer[audio_read];
            if (a >  1.0) a =  1.0;
            if (a < -1.0) a = -1.0;
            *buffer++ = a;
            audio_read = (audio_read+1)%ABUFSZ;

	    // other channel
	    //*buffer++ = *(buffer-1);
        }
    }

    return 0;
}

void audio_add_sample(double sample)
{
    audio_buffer[audio_write] = sample;
    audio_write = (audio_write + 1) % ABUFSZ;
}

void init_audio()
{
    rtaudio = new RtAudio();
    //Stk::setSampleRate(48000);

    RtAudio::StreamParameters parameters;
    parameters.deviceId = rtaudio->getDefaultOutputDevice();
    parameters.nChannels = 2;
    parameters.firstChannel = 0;
    unsigned int sampleRate = 48000;
    //unsigned int bufferFrames = 192;
    unsigned int bufferFrames = 512;

    RtAudio::StreamOptions options;
    options.flags = RTAUDIO_SCHEDULE_REALTIME;
    options.numberOfBuffers = 2;
    options.priority = SCHED_FIFO;

    memset(audio_buffer, 0, sizeof(double)*ABUFSZ);

    pthread_mutex_lock(&mutex);

    try {
        rtaudio->openStream( &parameters, NULL, RTAUDIO_FLOAT64,
                             sampleRate, &bufferFrames, &play, &options );
        rtaudio->startStream();
    }
    catch ( RtError& e ) {
        e.printMessage();
        delete rtaudio;
        rtaudio = 0;
    }
}

void *rec_thread_loop(void *arg)
{
  FILE *f = fopen("rec.raw","wb");
  if (!f) {
    printf("Warning: Could not open `rec.raw' for writing.\n");
    return (void*)1;
  }
  printf("Writing to `rec.raw'.\n");

  while (!quit) {
    if ( ((rec_read+1)%RECBUFSZ) == rec_write ) {
      Sleep(1);
    } else {
      double a = rec_buffer[rec_read];
      fwrite(&a, sizeof(double), 1, f);
      rec_read = (rec_read+1)%ABUFSZ;
    }
  }

  fclose(f);
  printf("\n\nClosed `rec.raw'\n\n");

  return 0;
}

void init_rec()
{
  pthread_create(&rec_thread, 0, rec_thread_loop, 0);
}

void cleanup_rec()
{
  pthread_join(rec_thread, 0);
}

void rec_add_sample(double sample)
{
    rec_buffer[rec_write] = sample;
    rec_write = (rec_write + 1) % RECBUFSZ;
}

float lp500(float vel)
{
    // Low-pass velocity (butterworth 2, 0.02)
    static float vx0=0, vx1=0, vx2=0, vy0=0, vy1=0, vy2=0;
    vx2 = vx1; vx1 = vx0; vx0 = vel;
    vy2 = vy1; vy1 = vy0;
    vy0 = vx0*0.000944691844 + vx1*0.001889383688 + vx2*0.000944691844
        + vy1*1.911197067426 + vy2*-0.914975834801;

    return vy0;
}

#if 0
float lp(float vel)
{
    static float vx0=0, vx1=0, vx2=0, vy0=0, vy1=0, vy2=0;
    vx2 = vx1; vx1 = vx0; vx0 = vel;
    vy2 = vy1; vy1 = vy0;
    vy0 = vx0*6.10061788e-05 + vx1*1.22012358e-04 + vx2*6.10061788e-05
        + vy1*1.97778648 + vy2*-0.97803051;

    return vy0;
}

float hp(float vel)
{
  // >>> scipy.signal.butter(2,0.005,'high')
    static float vx0=0, vx1=0, vx2=0, vy0=0, vy1=0, vy2=0;
    vx2 = vx1; vx1 = vx0; vx0 = vel;
    vy2 = vy1; vy1 = vy0;
    vy0 = vx0*0.98895425 + vx1*-1.9779085 + vx2*0.98895425
        + vy1*1.97778648 + vy2*-0.97803051;

    return vy0;
}
#endif
#if 1
float lp(float force)
{
  // >>> scipy.signal.butter(2,0.002,'low') 48Hz
	 static float fx0=0, fx1=0, fx2=0, fy0=0, fy1=0, fy2=0;
	 fx2 = fx1; fx1 = fx0; fx0 = force;
	 fy2 = fy1; fy1 = fy0;
	 fy0 = fx0*0.000009825917 + fx1*0.000019651834 + fx2*0.000009825917
		  + fy1*1.991114292202 + fy2*-0.991153595869;

    return fy0;
}

float hp(float force)
{
    // high-pass (butterworth 2, 0.002) 48 Hz
    static float fx0=0, fx1=0, fx2=0, fy0=0, fy1=0, fy2=0;
    fx2 = fx1; fx1 = fx0; fx0 = force;
    fy2 = fy1; fy1 = fy0;
    fy0 = fx0*0.995566972018 + fx1*-1.991133944035 + fx2*0.995566972018
        + fy1*1.991114292202 + fy2*-0.991153595869;

    return fy0;
}
#endif

void cleanup_audio()
{
    if (rtaudio)
        delete rtaudio;
    pthread_mutex_unlock(&mutex);
}

int main()
{
    cHapticDeviceHandler *h = new cHapticDeviceHandler();
    cGenericHapticDevice *dev;
    printf("Haptic devices found: %d\n", h->getNumDevices());

    int r = h->getDevice(dev, 0);
    if (r<0) {
      printf("h->getDevice() returned %d\n", r);
      return 1;
    }
    r = dev->open();
    if (r<0) {
      printf("dev->open() returned %d\n", r);
      return 1;
    }
    r = dev->initialize();
    if (r<0) {
      printf("dev->open() returned %d\n", r);
      return 1;
    }

    init_audio();
    //init_rec();

    signal(SIGINT, on_quit);

    printf("Initializing.\n");
    printf("Sample rate: %d\n", ALGO(init)(48000, 110.0f));

    float oldpos2d[2];

    printf("Running.\n");
    while (!quit)
    {
        cVector3d pos;
        dev->getPosition(pos);
        if (pos.z < -0.01) pos.z = -0.01;
        cVector3d force;
        force.zero();
        float newpos2d[2], pos2d[2], frc2d[2] = {0.0, 0.0}, snd2d[2] = {0.0, 0.0};
        float vn[1], xs[1], vel[2];

	newpos2d[0] = (float)(pos.y*1.0);
	newpos2d[1] = (float)(pos.z*1.0);
	frc2d[0] = 0;
	frc2d[1] = 0;
	float hpf, lpf;
	float vp = 0;

	int i, steps = 48000/1000;
	for (i=0; i<steps; i++)
	{
	  pos2d[0] = (newpos2d[0]-oldpos2d[0])/steps*i+oldpos2d[0];
	  pos2d[1] = (newpos2d[1]-oldpos2d[1])/steps*i+oldpos2d[1];
	  ALGO(step)(pos2d, frc2d, snd2d, vn, xs, vel);

	  hpf = hp(frc2d[0]);
	  lpf = lp(frc2d[0]);

	  audio_add_sample(snd2d[0]);
	  audio_add_sample(hpf*1000*10);

// 	  rec_add_sample(pos2d[0]);
// 	  rec_add_sample(snd2d[0]);
// 	  rec_add_sample(frc2d[0]);
// 	  rec_add_sample(hpf*1000*40);
// 	  rec_add_sample(lpf*1000*5);

// 	  static int t=0;
// 	  audio_add_sample(0);
// 	  audio_add_sample(sin(t++/48000.0*2*3.142159*1000));

	  if (aud_recording_buffer_pos < aud_recording_buffer_size) {
		   aud_recording_buffer[aud_recording_buffer_pos++] = hpf*1000*10;
		   aud_recording_buffer[aud_recording_buffer_pos++] = snd2d[0];
	  }
	}

	oldpos2d[0] = newpos2d[0];
	oldpos2d[1] = newpos2d[1];

	if (isnan(frc2d[0] || isnan(frc2d[1])))
	  force[0] = force[1] = force[2] = 0;
	else {
	  force.y = lpf*1000.0*10;
	  //force.y = frc2d[0]*1000.0*10;
	  force.z = frc2d[1]*-1000.0*5;
	}

#if 1
        static int c=0;
        if (c++ > 100) {

        printf("pos: %2.3f, %2.3f, %2.3f   frc: %2.3f, %2.3f, %2.3f        \r",
               pos[0], pos[1], pos[2], force[0], force[1], force[2]);
        fflush(stdout);
        c=0;
        }
#endif

#if 0
		static float anchor = 0, z = 0;
		static cVector3d old_pos(0,0,0);
		if (old_pos.z > 0 && pos.z < 0)
			 anchor = pos.y;
		z = pos.y-anchor;
		if (z > 0.001) z = 0.001;
		if (z < -0.001) z = -0.001;
		anchor = pos.y-z;
		if (pos.z < 0) {
			 force.y = (pos.y - anchor)*-1000;
		}
		old_pos = pos;
#endif

#if 0
		if (recording_buffer_pos < recording_buffer_size) {
			 recording_buffer[recording_buffer_pos++] = pos.y;
			 recording_buffer[recording_buffer_pos++] = force.y;
		}
		else {
			 force.set(0,0,0);
			 dev->setForce(force);

			 int i=0, j=0, n=0;
			 while (i<recording_buffer_size && j<aud_recording_buffer_size)
			 {
				  for (n=0; n<48 && j<aud_recording_buffer_size; n++) {
					   fprintf(stderr, "%g, %g, %g, %g\n",
							   recording_buffer[i],
							   recording_buffer[i+1],
							   aud_recording_buffer[j],
							   aud_recording_buffer[j+1]);
					   j+=2;
				  }
				  i+=2;
			 }
			 exit(0);
		}
#endif

		//force.set(0,0,0);
        dev->setForce(force);

        /* timer function */
        do_sleep();
    }

    cleanup_audio();
    //cleanup_rec();

    return 0;
}
