
#ifndef _VELSTATE_H_
#define _VELSTATE_H_

extern int sample_and_hold_pulse;
extern int limit_pulse;

int init_velstate(int requested_sample_rate, float string_frequency);

void step_velstate(float pos[2], float force[2], float sound[2],
                   float vn[1], float xs[1], float vel[2]);

#endif // _VELSTATE_H_
