#!/usr/bin/env python

from pylab import *
from scikits.audiolab import play
import os

modes = arange(40,220,20)
modes = [80]

signals = [loadtxt(os.popen('./sim cordis bow 10000 sndl')) for m in modes]
sr = 44100.0

for n,s in enumerate(signals):
    subplot(len(signals),1,n+1)
    plot(arange(len(s))/sr, s)

show()

play(signals[0], 44100)
