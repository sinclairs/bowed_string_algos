# Bowed string code from PhD thesis

## Summary

Code from my PhD thesis published in 2013.  My digital waveguide bowed
string algorithms are here, all reimplementations or modifications
from the [Synthesis Toolkit in C++](http://github.com/thestk/stk) as
developed by Julius O. Smith III, Perry Cook, and Gary Scavone, as
well as copies of RtAudio by Gary Scavone, please see related
copyrights.  Some related code from the ERGOS system bowed string
modal bowed string implementation from ACROE (by Jean Loup Florens) is
omitted because I don't have redistribution rights.

This principle focus of this work was investigating into methods for
real-time coupling of force feedback haptic devices with these models.

The thesis can be found at
[PDF](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.592.7101)
and was developed under the supervision of Marcelo M. Wanderley at the
[IDMIL](http://idmil.org), with co-supervision by Vincent Hayward.
Code for a related investigation into velocity estimators can be found
in a [sister repository](http://github.com/radarsat1/velocity).

## Instructions

For now I apologize for leaving this very poorly documented, but
basically it generates a simulator called `sim` that runs various
bowed string algorithms and generates text files with traces of their
inputs and outputs.  A few options are available to turn features
(more like experiments) on and off.  I plan to come back to this to
document what each algorithm name and option represents, but note that
several parameters can only be changed by modifying `sim.c`.  The code
is written in C and is designed in a certain way because it was
included in several contexts, one of which was to embed it in firmware
for a high-frequency real-time control DSP card.  Some code is left
here in which it was used to interface with commercial haptic devices
through an old version of the [CHAI 3D](http://chai3d.org) software.

## License

(C) 2018 Stephen Sinclair <radarsat1@gmail.com>

Published under LGPL 3 or above.  Please see LICENSE, but unofficial
summary: do with it what you want in your own projects but if you want
to distribute a closed copy or derivative please contact me.  (Maybe
we can work together!)
