
#ifndef _TWOPOINT_H_
#define _TWOPOINT_H_

int init_twopoint(int requested_sample_rate, float string_frequency);

void step_twopoint(float pos[2], float force[2], float sound[2],
                   float vn[1], float xs[1], float vel[2]);

#endif // _TWOPOINT_H_
