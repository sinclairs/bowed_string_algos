#!/usr/bin/env python

from pylab import *

partials = arange(1,201)

amplitudes = 1.0/(partials*1.11035156)

clf()
plot(partials, amplitudes * (200-partials)/200)

show()
